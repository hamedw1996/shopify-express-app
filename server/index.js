const dotenv = require('dotenv').config();
const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

const api = require('./router/api');
const shopify = require('./router/shopify');
const utils = require('./src/recommendations/utils');


const port = 3000;

app.use(bodyParser.json());
app.set('views', path.join(__dirname, 'views'));
app.set("view engine", "ejs");

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
      //respond with 200
      res.send(200);
    }
    else {
    //move on
      next();
    };
})

app.use('/dist', express.static(path.join(__dirname, '../client/dist')));
app.use('/api', api);
app.use('/shopify', shopify);

app.get('/', (req, res) => {
    res.send("Hello World!");
});
app.get('/generate-boxes', (req, res) => {
  const boxes = utils.generateScoreReport(req.query.sort_by);
  res.render('box_report', { boxes });
})

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/dist/index.html'));
})


app.listen(port, () => {console.log(`Example app listening on port ${port}!`);});