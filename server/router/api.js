const express = require('express');
const utils = require('../src/recommendations/utils');

const router = express.Router();

router.post('/boxes/results', (req, res) => {
  const users = utils.calculateBoxScoreFinal(req.body);
  const boxSize = utils.calculateBoxSize(req.body);
  res.json({ users, boxSize });
})

module.exports = router;