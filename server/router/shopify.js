const express = require('express');
const crypto = require('crypto');
const cookie = require('cookie');
const nonce = require('nonce')();
const querystring = require('querystring');
const request = require('request-promise');
const axios = require('axios');
const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const accessTokenPerm = process.env.SHOPIFY_ACCESS_TOKEN;
const forwardingAddress = process.env.FORWARDING_ADDRESS;
const scopes = 'read_products,read_customers,write_customers';

const userUtils = require('../src/recommendations/users');
const categories = require('../src/recommendations/catalog');

const router = express.Router();

router.get('/', (req, res) => {
  const shop = req.query.shop;
  if (shop) {
      const state = nonce();
      const redirectUri = forwardingAddress + '/shopify/callback';
      const installUrl = 'https://' + shop + '/admin/oauth/authorize?client_id=' + apiKey + '&scope=' +
          scopes + '&state=' + state + '&redirect_uri=' + redirectUri;
      res.cookie('state', state);
      res.redirect(installUrl);
  } else {
      return res.status(400).send("Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request");
  }
});

router.post('/:shop/customer', (req, res) => {
  // Create new customer with metafields
  const shopRequestHeaders = {
    'X-Shopify-Access-Token': accessTokenPerm,
    'Content-type': 'application/json'
  };
  const customerURL = 'https://' + req.params.shop + '/admin/api/2020-07/customers.json';
  const customerPayload = userUtils.generateCustomerPayload(req.body, categories);
  axios({
    method: 'POST',
    url: customerURL,
    headers: shopRequestHeaders,
    data: JSON.stringify(customerPayload)
  }).then(function (success) {
      console.log("success " + success)
      res.json({ message: "success" })
  }).catch(function (err) {
      console.log("err " + err)
      res.send(err)
  });
});

router.post('/:shop/survey', (req, res) => {
  // Update preferences for customer
  const shopRequestHeaders = {
    'X-Shopify-Access-Token': accessTokenPerm,
    'Content-type': 'application/json'
  };
  if (req.query.customer_id) {
    const metafieldURL = 'https://' + req.params.shop + '/admin/api/2020-07/customers/' + req.query.customer_id + '/metafields.json';
    let metafieldPayload = {
        metafield: {
        namespace: "survey",
        key: "preferences",
        value: JSON.stringify(req.body),
        value_type: "json_string"
      }
    }
    axios({
      method: 'POST',
      url: metafieldURL,
      headers: shopRequestHeaders,
      data: JSON.stringify(metafieldPayload)
    }).then(function (success) {
        console.log("success " + success)
        res.json({ message: "success" })
    }).catch(function (err) {
        console.log("err " + err)
        res.json({ message: "failed" })
    });
  } else {
    console.log("Customer ID doesn't exist")
    res.json({ message: "failed" })
  }
});

router.get('/callback', (req, res) => {
  const { shop, hmac, code, state } = req.query;

  if (shop && hmac && code) {
      const map = Object.assign({}, req.query);
      delete map['signature'];
      delete map['hmac'];
      const message = querystring.stringify(map);
      const providedHmac = Buffer.from(hmac, 'utf-8');
      const generatedHash = Buffer.from(
          crypto
              .createHmac('sha256', apiSecret)
              .update(message)
              .digest('hex'),
          'utf-8'
      );
      let hashEquals = false;

      try {
          hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
      } catch (e) {
          hashEquals = false;
      };

      if (!hashEquals) {
          return res.status(400).send("HMAC validation failed");
      }

      const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token';
      const accessTokenPayload = {
          client_id: apiKey,
          client_secret: apiSecret,
          code,
      };
      request.post(accessTokenRequestUrl, { json: accessTokenPayload })
          .then((accessTokenResponse) => {
              const accessToken = accessTokenResponse.access_token;
              const shopRequestUrl = 'https://' + shop + '/admin/api/2020-01/shop.json';
              const shopRequestHeaders = {
                  'X-Shopify-Access-Token': accessToken,
              };

              request.get(shopRequestUrl, { headers: shopRequestHeaders })
                  .then((shopResponse) => {
                      res.end(shopResponse);
                  })
                  .catch((error) => {
                      res.status(error.statusCode).send(error.error.error_description);
                  });
          })
          .catch((error) => {
              res.status(error.statusCode).send(error.error.error_description);
          });
  } else {
      res.status(400).send('Required parameters missing');
  }
});

module.exports = router;