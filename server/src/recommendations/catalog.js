module.exports = {
  "topically": [
    "lotions/salves",
    "oil",
    "patches",
    "spray",
    "soaks"
  ],
  "pets": [
    "cats",
    "dogs"
  ],
  "ingesting": [
    "edibles",
    "beverages",
    "capsules",
    "tinctures"
  ],
  "vaping": [
    "oil",
    "wax"
  ]
}

// {
//   "topically": [
//     { "lotions/salves": 1 },
//     { "oil": 1 },
//     { "patches": 1 },
//     { "spray": 1 },
//     { "soaks": 0 }
//   ],
//   "pets": [
//     "all";
//   ],
//   "ingesting": [
//     "edibles",
//     "beverages",
//     "capsules",
//     "tinctures"
//   ],
//   "vaping": [
//     "oil",
//     "wax"
//   ]
// }