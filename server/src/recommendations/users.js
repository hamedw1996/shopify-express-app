const generateCustomerPayload = (fields, categories) => {
  let metafields = [];
  let customer = fields["customer"];
  customer["password_confirmation"] = customer["password"];
  let preferences = formatPreferencesPayload(fields["preferences"], categories);
  Object.keys(fields).forEach((key) => {
    if (key === "customer" || (key === "form_type" || key === "utf8")) {return}
    else {
      metafields.push({
        key: key,
        value: (key === "preferences") ? preferences : (Array.isArray(fields[key]) ? fields[key].join(',') : fields[key]),
        value_type: (key === "preferences") ? "json_string" : "string",
        namespace: "survey"
      });
    }
  });
  fullMetafields = {metafields: metafields}
  return {"customer": Object.assign({}, customer, fullMetafields)};
}

const formatPreferencesPayload = (productTypes, categories) => {
  let preferencePayload = {};
  Object.keys(categories).forEach((cat) => {
    let pref = (productTypes.includes(cat.toLowerCase())) ? 1 : 0
    preferencePayload[cat] = {}
    categories[cat].forEach((subCat) => {
      preferencePayload[cat][subCat] = pref;
    })
  });
  return JSON.stringify(preferencePayload);
}

module.exports = {
  generateCustomerPayload
}