const catalog = require('./catalog.js');
const faker = require('faker');
const users = require('../users.json');

const generateUsers = (count = 100) => {
  const newUsers = []
  for (let index = 0; index < count; index++) {
    newUsers.push(generateBaseUser());
  }
  return newUsers
}

const pickPreferences = () => {
  const userPreferences = {};
  const categories = Object.keys(catalog);
  categories.forEach(category => {
    userPreferences[category] = {}
    catalog[category].forEach(subcategory => {
      userPreferences[category][subcategory] = getRandomInt(2);
    })
  })
  return userPreferences;
}

const generateBaseUser = () => {
  return {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    preferences: pickPreferences()
  }
}

const aggregateUserPreferences = (preferences = []) => {
  const preferenceMap = {};
  preferences.forEach((preference) => {
    Object.keys(preference).forEach((category) => {
      if (!preferenceMap[category]) {
        preferenceMap[category] = {};
      }
      const categoryPreferences = preference[category];
      Object.keys(categoryPreferences).forEach((subcategory) => {
        const preference = categoryPreferences[subcategory]
        if (!preferenceMap[category][subcategory]) {
          preferenceMap[category][subcategory] = {};
        }

        if (!preferenceMap[category][subcategory][preference]) {
          preferenceMap[category][subcategory][preference] = 0;
        }

        preferenceMap[category][subcategory][preference]++;
      })
    })
  });
  return preferenceMap
}

const boxCategoryMap = {};

const generateBoxes = () => {
  Object.keys(catalog).forEach(category => {
    const categoryBoxCombs = generateBoxTypeCombs(category);
    boxCategoryMap[category] = categoryBoxCombs;
  })
  const boxArchetypes = generateBoxArchetypes();
  var boxes = [];
  boxArchetypes.forEach((boxArchetype, ind) => {
    categories = boxArchetype.split(';');
    boxes.push(generateCategoryBox(categories));
  });
  return boxes;
}

const generateBoxArchetypes = () => {
  var categoryCombs = []
  Object.keys(catalog).forEach((category, index) => {
    Object.keys(catalog).forEach((category2, index2) => {
      if (index != index2) {
          if (category[0] < category2[0]) {
            if (!(categoryCombs.includes(category+";"+category2))) {
              categoryCombs.push(category+";"+category2)
            }
          } else {
            if (!(categoryCombs.includes(category2+";"+category))) {
              categoryCombs.push(category2+";"+category)
            }            
          }
        }
    });
  });
  return categoryCombs; 
}

const generateCategoryBox = ([category1, category2]) => {
  const boxes = [];
  const boxSubcategories1 = boxCategoryMap[category1];
  const boxSubcategories2 = boxCategoryMap[category2];
  boxSubcategories1.forEach(subBox1 => {
    boxSubcategories2.forEach(subBox2 => {
      const box = {
        [category1]: subBox1,
        [category2]: subBox2
      }
      boxes.push(box);
    });
  });
  return boxes
}

const testBox = () => {

};

const generateBoxTypeCombs = (category) => {
  var usedCombs = [];
  for (let i = 0; i < catalog[category].length; i++) {
    for (let j = 0; j < catalog[category].length; j++) {
      if (i != j) {
        const combinationKey1 = catalog[category][i]+";"+catalog[category][j]
        const combinationKey2 = catalog[category][j]+";"+catalog[category][i]
        if (!(
          usedCombs.includes(combinationKey1) || 
          usedCombs.includes(combinationKey2)
         )) {
          usedCombs.push(combinationKey1);
        }
      }
    };
  };
  return usedCombs;
}

const getRandomInt = (max) => {
  return Math.floor(Math.random() * (max + 1));
}

const generateUserReport = () => {
  // const users = generateUsers();
  const preferences = users.map(user => user.preferences);
  return aggregateUserPreferences(preferences);
}

const generateScoreReport = (sortBy='averageScore') => {
  const boxes = generateBoxes().reduce((acc, val) => acc.concat(val), []);
  return aggregateBoxScores(boxes, users, sortBy);
}

const aggregateBoxScores = (boxes, users, sortBy) => {
  boxes.forEach((box) => {
    const score = calculateBoxScore(box, users);
    box.score = score;
  });
  return boxes.sort((a, b) => (a.score[sortBy] > b.score[sortBy]) ? -1 : 1)
}

const calculateBoxScore = (box, users) => {
  const scores = [];
  users.forEach(user => {
    const score = calculateBoxScoreForUser(box, user);
    scores.push(score);
  })
  return generateBoxStats(scores);
}

const calculateBoxScoreFinal = (box) => {
  var fullUsers = []
  const boxSize = calculateBoxSize(box)
  users.forEach(user => {
    const userScore = calculateBoxScoreForUser(box, user);
    user['boxScore'] = userScore
    user['satisfied'] = calculateSatisfactionForUser(boxSize, userScore);
    fullUsers.push(user)
  });
  return fullUsers;
}

const generateBoxStats = (scores, boxSize = 4) => {
  return {
    averageScore: scores.reduce((a,b) => a + (b/boxSize), 0) / scores.length,
    satisfiedUsers: scores.filter(score => score >= boxSize).length
  }
}

const calculateBoxScoreForUser = (box, user) => {
  let boxScore = 0;
  Object.keys(box).forEach(category => {
    const boxItems = box[category].split(';');
    userCategoryPreference = user.preferences[category];
    let categoryScore = 0;
    boxItems.forEach(item => categoryScore += userCategoryPreference[item]);
    boxScore += categoryScore 
  });
  return boxScore
}

const calculateBoxSize = (box) => {
  var boxSize = 0;
  Object.keys(box).forEach(category => {
    items = box[category].split(";");
    boxSize += items.length;
  });
  return boxSize;
}

const calculateSatisfactionForUser = (boxSize, boxScore) => {
  return ((boxScore / boxSize) >= 1) ? true : false
}

module.exports = {
  generateUserReport,
  generateScoreReport,
  calculateBoxScoreFinal,
  calculateBoxSize
}