#!/bin/bash
# Install node.js and Forever.js
sudo apt-get update
sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -       
sudo apt -y install nodejs make gcc g++
# sudo apt-get install nodejs -y
sudo apt-get install npm -y
sudo npm install forever -g
sudo rm -rf /home/ubuntu/my-app1/