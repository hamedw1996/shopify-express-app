#!/bin/bash
# Stop all servers and start the server
sudo forever stopall
chmod +x /home/unbuntu/.env.sh
. /home/ubuntu/.env.sh
cd /home/ubuntu/my-app1/client
sudo npm run build:client
cd /home/ubuntu/my-app1/server
sudo SHOPIFY_API_KEY=$SHOPIFY_API_KEY SHOPIFY_API_SECRET=$SHOPIFY_API_SECRET SHOPIFY_ACCESS_TOKEN=$SHOPIFY_ACCESS_TOKEN FORWARDING_ADDRESS=$FORWARDING_ADDRESS forever start /home/ubuntu/my-app1/server/index.js