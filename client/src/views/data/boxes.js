export default {
  users: [
    {
      firstName: 'Ivah',
      lastName: 'Wiegand',
      email: 'Blair86@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 2,
          patches: 1,
          spray: 1,
          soaks: 0
        },
        pets: {
          cats: 1,
          dogs: 0
        },
        ingesting: {
          edibles: 2,
          beverages: 1,
          capsules: 2,
          tinctures: 1
        },
        vaping: {
          oil: 2,
          wax: 2
        }
      },
      boxScore: 7,
      satisfied: true
    },
    {
      firstName: 'Delores',
      lastName: 'Boehm',
      email: 'Muhammad94@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 0,
          patches: 0,
          spray: 0,
          soaks: 2
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 0,
          beverages: 0,
          capsules: 2,
          tinctures: 2
        },
        vaping: {
          oil: 0,
          wax: 2
        }
      },
      boxScore: 4,
      satisfied: false
    },
    {
      firstName: 'Gilberto',
      lastName: 'Schultz',
      email: 'River_OKeefe75@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 1,
          patches: 0,
          spray: 0,
          soaks: 2
        },
        pets: {
          cats: 1,
          dogs: 2
        },
        ingesting: {
          edibles: 1,
          beverages: 0,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 7,
      satisfied: true
    },
    {
      firstName: 'Zoie',
      lastName: 'Hahn',
      email: 'Bettie.Kilback90@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 1,
          patches: 0,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 2
        },
        ingesting: {
          edibles: 2,
          beverages: 1,
          capsules: 0,
          tinctures: 0
        },
        vaping: {
          oil: 2,
          wax: 2
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Ariane',
      lastName: 'Olson',
      email: 'Kathryne_Schuppe9@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 2,
          oil: 0,
          patches: 0,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 2,
          beverages: 0,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 1,
          wax: 1
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Anika',
      lastName: 'Erdman',
      email: 'Addison.Gibson@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 1,
          patches: 2,
          spray: 1,
          soaks: 2
        },
        pets: {
          cats: 1,
          dogs: 0
        },
        ingesting: {
          edibles: 2,
          beverages: 1,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 0,
          wax: 1
        }
      },
      boxScore: 2,
      satisfied: false
    },
    {
      firstName: 'Cristal',
      lastName: 'Beier',
      email: 'Laverne.Schaefer@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 1,
          spray: 0,
          soaks: 0
        },
        pets: {
          cats: 2,
          dogs: 2
        },
        ingesting: {
          edibles: 0,
          beverages: 0,
          capsules: 1,
          tinctures: 2
        },
        vaping: {
          oil: 0,
          wax: 2
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Madge',
      lastName: "O'Conner",
      email: 'Clyde2@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 1,
          patches: 1,
          spray: 1,
          soaks: 2
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 2,
          beverages: 1,
          capsules: 0,
          tinctures: 0
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 4,
      satisfied: false
    },
    {
      firstName: 'Delaney',
      lastName: 'Monahan',
      email: 'Braden.Kozey@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 1,
          patches: 2,
          spray: 2,
          soaks: 1
        },
        pets: {
          cats: 1,
          dogs: 1
        },
        ingesting: {
          edibles: 2,
          beverages: 1,
          capsules: 1,
          tinctures: 1
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 6,
      satisfied: true
    },
    {
      firstName: 'Elyssa',
      lastName: 'Ernser',
      email: 'Enrique.Gislason76@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 1,
          patches: 1,
          spray: 0,
          soaks: 0
        },
        pets: {
          cats: 1,
          dogs: 1
        },
        ingesting: {
          edibles: 0,
          beverages: 1,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 0,
          wax: 0
        }
      },
      boxScore: 4,
      satisfied: false
    },
    {
      firstName: 'Tony',
      lastName: 'Bauch',
      email: 'Kariane.Harvey77@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 1,
          spray: 1,
          soaks: 2
        },
        pets: {
          cats: 2,
          dogs: 2
        },
        ingesting: {
          edibles: 0,
          beverages: 1,
          capsules: 2,
          tinctures: 2
        },
        vaping: {
          oil: 2,
          wax: 2
        }
      },
      boxScore: 8,
      satisfied: true
    },
    {
      firstName: 'Abraham',
      lastName: 'Dibbert',
      email: 'Bailee.Schuster45@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 0,
          patches: 2,
          spray: 2,
          soaks: 0
        },
        pets: {
          cats: 0,
          dogs: 1
        },
        ingesting: {
          edibles: 0,
          beverages: 0,
          capsules: 2,
          tinctures: 0
        },
        vaping: {
          oil: 1,
          wax: 0
        }
      },
      boxScore: 4,
      satisfied: false
    },
    {
      firstName: 'Etha',
      lastName: 'Spencer',
      email: 'Adrian_Waters@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 0,
          patches: 1,
          spray: 0,
          soaks: 0
        },
        pets: {
          cats: 0,
          dogs: 2
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 1,
          wax: 0
        }
      },
      boxScore: 4,
      satisfied: false
    },
    {
      firstName: 'Alexzander',
      lastName: 'Heathcote',
      email: 'Ezra.Konopelski@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 0,
          spray: 2,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 2
        },
        ingesting: {
          edibles: 0,
          beverages: 0,
          capsules: 0,
          tinctures: 2
        },
        vaping: {
          oil: 2,
          wax: 0
        }
      },
      boxScore: 6,
      satisfied: true
    },
    {
      firstName: 'Durward',
      lastName: 'Rowe',
      email: 'Trace_Satterfield71@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 0,
          patches: 2,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 2
        },
        ingesting: {
          edibles: 2,
          beverages: 2,
          capsules: 1,
          tinctures: 1
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 6,
      satisfied: true
    },
    {
      firstName: 'Electa',
      lastName: 'Douglas',
      email: 'Velda12@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 2,
          patches: 2,
          spray: 1,
          soaks: 0
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 1,
          beverages: 1,
          capsules: 2,
          tinctures: 2
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 8,
      satisfied: true
    },
    {
      firstName: 'Rhett',
      lastName: 'Wolff',
      email: 'Eulah28@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 0,
          spray: 1,
          soaks: 1
        },
        pets: {
          cats: 1,
          dogs: 1
        },
        ingesting: {
          edibles: 1,
          beverages: 0,
          capsules: 0,
          tinctures: 1
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Dominique',
      lastName: 'Heidenreich',
      email: 'Gudrun.Schaden87@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 0,
          patches: 0,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 0,
          dogs: 0
        },
        ingesting: {
          edibles: 1,
          beverages: 0,
          capsules: 0,
          tinctures: 1
        },
        vaping: {
          oil: 0,
          wax: 2
        }
      },
      boxScore: 1,
      satisfied: false
    },
    {
      firstName: 'Queen',
      lastName: 'Waters',
      email: 'Damion.Lueilwitz11@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 1,
          patches: 2,
          spray: 2,
          soaks: 1
        },
        pets: {
          cats: 0,
          dogs: 2
        },
        ingesting: {
          edibles: 2,
          beverages: 0,
          capsules: 2,
          tinctures: 0
        },
        vaping: {
          oil: 2,
          wax: 2
        }
      },
      boxScore: 7,
      satisfied: true
    },
    {
      firstName: 'Erich',
      lastName: 'Lakin',
      email: 'Sabryna_Goldner92@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 2,
          oil: 2,
          patches: 1,
          spray: 1,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 2,
          tinctures: 2
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 9,
      satisfied: true
    },
    {
      firstName: 'Larue',
      lastName: 'Becker',
      email: 'Marcus_Lehner33@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 2,
          oil: 2,
          patches: 0,
          spray: 2,
          soaks: 2
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 2,
          tinctures: 1
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 9,
      satisfied: true
    },
    {
      firstName: 'Sigmund',
      lastName: 'Wiza',
      email: 'Kasandra_Schinner5@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 0,
          patches: 1,
          spray: 1,
          soaks: 0
        },
        pets: {
          cats: 1,
          dogs: 0
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 2,
          tinctures: 2
        },
        vaping: {
          oil: 1,
          wax: 0
        }
      },
      boxScore: 3,
      satisfied: false
    },
    {
      firstName: 'Hillard',
      lastName: 'Connelly',
      email: 'Dorothy.Renner83@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 0,
          spray: 0,
          soaks: 0
        },
        pets: {
          cats: 1,
          dogs: 0
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 1,
          tinctures: 2
        },
        vaping: {
          oil: 0,
          wax: 1
        }
      },
      boxScore: 3,
      satisfied: false
    },
    {
      firstName: 'Aisha',
      lastName: 'Roberts',
      email: 'Millie.Pfannerstill@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 2,
          spray: 2,
          soaks: 2
        },
        pets: {
          cats: 2,
          dogs: 0
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 2,
          wax: 2
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Edmond',
      lastName: 'Zieme',
      email: 'Antone.Price@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 0,
          patches: 2,
          spray: 2,
          soaks: 0
        },
        pets: {
          cats: 1,
          dogs: 2
        },
        ingesting: {
          edibles: 0,
          beverages: 2,
          capsules: 1,
          tinctures: 1
        },
        vaping: {
          oil: 2,
          wax: 1
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Anastasia',
      lastName: 'Olson',
      email: 'Vallie_Eichmann10@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 2,
          oil: 0,
          patches: 1,
          spray: 2,
          soaks: 0
        },
        pets: {
          cats: 0,
          dogs: 2
        },
        ingesting: {
          edibles: 1,
          beverages: 0,
          capsules: 0,
          tinctures: 2
        },
        vaping: {
          oil: 0,
          wax: 2
        }
      },
      boxScore: 4,
      satisfied: false
    },
    {
      firstName: 'Shanelle',
      lastName: 'Johns',
      email: 'Graciela98@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 0,
          oil: 2,
          patches: 0,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 1,
          dogs: 0
        },
        ingesting: {
          edibles: 0,
          beverages: 0,
          capsules: 2,
          tinctures: 1
        },
        vaping: {
          oil: 1,
          wax: 1
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Riley',
      lastName: 'Schroeder',
      email: 'Rebeca.Lynch71@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 2,
          oil: 2,
          patches: 0,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 0,
          beverages: 2,
          capsules: 1,
          tinctures: 0
        },
        vaping: {
          oil: 1,
          wax: 0
        }
      },
      boxScore: 7,
      satisfied: true
    },
    {
      firstName: 'Paxton',
      lastName: 'Schumm',
      email: 'Felipa_Gibson@yahoo.com',
      preferences: {
        topically: {
          'lotions/salves': 2,
          oil: 0,
          patches: 2,
          spray: 1,
          soaks: 1
        },
        pets: {
          cats: 2,
          dogs: 1
        },
        ingesting: {
          edibles: 2,
          beverages: 2,
          capsules: 0,
          tinctures: 1
        },
        vaping: {
          oil: 0,
          wax: 2
        }
      },
      boxScore: 3,
      satisfied: false
    },
    {
      firstName: 'Wade',
      lastName: "O'Keefe",
      email: 'Clement.McCullough89@hotmail.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 1,
          patches: 2,
          spray: 0,
          soaks: 2
        },
        pets: {
          cats: 1,
          dogs: 0
        },
        ingesting: {
          edibles: 1,
          beverages: 2,
          capsules: 2,
          tinctures: 0
        },
        vaping: {
          oil: 1,
          wax: 2
        }
      },
      boxScore: 5,
      satisfied: true
    },
    {
      firstName: 'Frederick',
      lastName: 'Bogisich',
      email: 'Josefina_Kub9@gmail.com',
      preferences: {
        topically: {
          'lotions/salves': 1,
          oil: 0,
          patches: 0,
          spray: 0,
          soaks: 1
        },
        pets: {
          cats: 0,
          dogs: 1
        },
        ingesting: {
          edibles: 1,
          beverages: 1,
          capsules: 1,
          tinctures: 1
        },
        vaping: {
          oil: 1,
          wax: 2
        }
      },
      boxScore: 4,
      satisfied: false
    }
  ]
}
